#!/usr/bin/env python3

import os
import sys
import argparse

import pikepdf

parser = argparse.ArgumentParser(
    prog='suppr.py',
    description='Supprime le filigrane de Télérecours'
)

parser.add_argument(
    'input',
    action='store',
    help='fichier d\'entrée'
)

parser.add_argument(
    'output',
    action='store',
    help='fichier de sortie'
)

args = parser.parse_args()

input_path = args.input
output_path = args.output
root_path = os.path.dirname(os.path.abspath(__file__))

if not input_path.startswith('/'):
    input_path = os.path.abspath(root_path + '/' + input_path)

if not output_path.startswith('/'):
    output_path = os.path.abspath(root_path + '/' + output_path)

input_file = pikepdf.open(input_path)
output_file = pikepdf.Pdf.new()

found = False

def is_watermark(pdf_object):
    try:
        if (pdf_object.get('/Type') == '/XObject'
        and pdf_object.get('/Name') == '/Fm0'
        and pdf_object.get('/BBox')
        and pdf_object.get('/Filter') == '/FlateDecode'):
            return True
        else:
            return False
    except:
        return False

for pdf_object in input_file.objects:
    try:
        if not found and is_watermark(pdf_object):
            found = True
    except:
        pass

if not found:
    print('Impossible de trouver le filigrane.')
    sys.exit(1)
else:
    for page in input_file.pages:
        if is_watermark(input_file.pages[input_file.pages.index(page)].Resources.XObject['/Fm0']):
            del input_file.pages[input_file.pages.index(page)].Resources.XObject['/Fm0']
        else:
            print('Erreur lors de la suppression du filigrane')
            sys.exit(1)
    input_file.remove_unreferenced_resources()
    input_file.save(output_path)

