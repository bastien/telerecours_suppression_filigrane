# README

Il s'agit d'un script qui supprime les filigranes ajoutés par Télérecours. Il devrait également fonctionner avec Télérecours Citoyens.

## Installation

Il est recommandé d'utiliser virtualenv :

```bash
virtualenv --python=/usr/bin/python3 .
source bin/activate
pip3 install -r requirements.txt
```

## Utilisation

```bash
suppr.py <input.pdf> <output.pdf>
```

## Licence

[CeCILL_V2.1-fr](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html) (voir le fichier `LICENSE`)
